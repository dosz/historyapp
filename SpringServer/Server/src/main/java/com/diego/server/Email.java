package com.diego.server;

import javax.persistence.*;

/**
 * Created by dos_6 on 22/05/2016.
 */

@Entity
@Table(name = "mails")
public class Email {

    @Id
    @GeneratedValue
    private int id;
    @Column
    private String mail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
