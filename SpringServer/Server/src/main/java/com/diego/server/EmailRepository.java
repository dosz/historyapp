package com.diego.server;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by dos_6 on 22/05/2016.
 */
public interface EmailRepository extends CrudRepository<Email,String> {

    List<Email> findAll();

}
