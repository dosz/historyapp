package com.diego.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailController {

    @Autowired
    private EmailRepository repository;

    @RequestMapping("/add_email")
    public void addEmail(@RequestParam(value = "mail", defaultValue = "nada") String mail){

       /* Opinion opinion = new Opinion();
        opinion.setTitulo(titulo);
        opinion.setTexto(texto);
        opinion.setFecha(new Date(System.currentTimeMillis()));
        opinion.setPuntuacion(puntuacion);

        repository.save(opinion);*/

        Email correo = new Email();
        correo.setMail(mail);
        repository.save(correo);

    }
}
