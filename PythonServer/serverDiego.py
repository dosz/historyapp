from flask import Flask
import json
import MySQLdb as mysql

app = Flask(__name__)


@app.route('/checkmail/mail=<string:mail>', methods=['GET', 'POST'])
def urlcheckmail(mail):
    data = {}
    data['check'] = checkmail(mail)
    return json.dumps(data)


@app.route('/setmail/mail=<string:mail>', methods=['GET', 'POST'])
def urlsetmail(mail):
    data = {}
    data['insert'] = setmail(mail)
    return json.dumps(data)


def checkmail(mail):
    cnx = connect()
    cursor = cnx.cursor()
    cursor.execute("SELECT * FROM mails WHERE mail = '%s'" % (mail))
    row = cursor.fetchone()
    if row != None:
        return True
    else:
        return False
    cnx.close


def setmail(mail):
    cnx = connect()
    cursor = cnx.cursor()
    try:
        cursor.execute(
            "INSERT INTO mails (mail) VALUES ('%s')" % (mail))
        cnx.commit()
    except:
        cnx.rollback()
        return False
    return True
    cnx.close


def connect():
    return mysql.connect('127.0.0.1', 'root', '', 'servidor', port=3306)


if __name__ == '__main__':
    app.run(host='10.0.0.1', debug='true')