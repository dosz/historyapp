package com.project.diego.historyapp.main;

import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import com.project.diego.historyapp.R;
import com.project.diego.historyapp.adapters.FavoritoAdapter;
import com.project.diego.historyapp.adapters.MonumentoAdapter;
import com.project.diego.historyapp.utils.DataBase;
import com.project.diego.historyapp.utils.Monumento;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends ActionBarActivity implements AdapterView.OnItemClickListener{

    private ListView lista;
    private ListView listaFavoritos;
    private ArrayList<Monumento> listaMonumentos;
    private ArrayList<Monumento> listaFavsMonumentos;
    private ArrayList<String> favsArray;
    private MonumentoAdapter adapter;
    private FavoritoAdapter favoritoAdapter;
    private DataBase dbf;
    SQLiteDatabase db;

    private static final String URL = "http://www.zaragoza.es/georref/json/hilo/ver_Monumento";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        favsArray = new ArrayList<String>();

        Resources res = getResources();

        TabHost tabs = (TabHost) findViewById(android.R.id.tabhost);
        tabs.setup();

        TabHost.TabSpec spec = tabs.newTabSpec("mitab1");
        spec.setContent(R.id.tab1);
        spec.setIndicator("", res.getDrawable(android.R.drawable.ic_btn_speak_now));
        tabs.addTab(spec);

        spec = tabs.newTabSpec("mitab2");
        spec.setContent(R.id.tab2);
        spec.setIndicator("", res.getDrawable(android.R.drawable.btn_star));
        tabs.addTab(spec);

        tabs.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {

            }
        });

         tabs.setCurrentTab(0);


        lista = (ListView) findViewById(R.id.lvMonumentos);
        lista.setOnItemClickListener(this);
        listaMonumentos = new ArrayList<Monumento>();
        adapter = new MonumentoAdapter(this, R.layout.fila, listaMonumentos);
        lista.setAdapter(adapter);

        listaFavoritos = (ListView) findViewById(R.id.lvFavoritos);
        listaFavoritos.setOnItemClickListener(this);
        listaFavsMonumentos = new ArrayList<Monumento>();
        favoritoAdapter = new FavoritoAdapter(this, R.layout.fila, listaFavsMonumentos);
        listaFavoritos.setAdapter(favoritoAdapter);
        //getFavs();

        registerForContextMenu(lista);
        registerForContextMenu(listaFavoritos);

        dbf = new DataBase(this, "DBFavoritos", null, 1);

        db = dbf.getWritableDatabase();

        cargarMonumentos();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();

        if(v.getId() == R.id.lvMonumentos){
            inflater.inflate(R.menu.menu_mumento, menu);
        }else if(v.getId() == R.id.lvFavoritos){
            inflater.inflate(R.menu.menu_favs, menu);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Intent intent = null;
        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.action_options:
                intent = new Intent(this, Info_Web.class);
                startActivity(intent);
                return true;
            case R.id.action_about:
                intent = new Intent(this, About.class);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onContextItemSelected(MenuItem item) {
        Monumento monumento;
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.addFavorites:
                monumento = listaMonumentos.get(info.position);
                Toast.makeText(getApplicationContext(), monumento.getNombre(), Toast.LENGTH_SHORT).show();

                //db.execSQL("INSERT INTO Favoritos (nombre) VALUES('" + monumento.getNombre() + "')");


                listaFavsMonumentos.add(monumento);

                listaFavoritos.deferNotifyDataSetChanged();
                return true;
            case R.id.showMap:

                monumento = listaMonumentos.get(info.position);
                Intent i = new Intent(this, Mapa.class);
                i.putExtra("latitud", monumento.getLatitud());
                i.putExtra("longitud", monumento.getLongitud());
                i.putExtra("nombre", monumento.getNombre());
                startActivity(i);
                return true;
            case R.id.showMapFav:
                Toast.makeText(getApplicationContext(),
                        "Próximamente", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.deleteFavorites:
                monumento = listaFavsMonumentos.get(info.position);
                listaFavsMonumentos.remove(monumento);
                listaFavoritos.deferNotifyDataSetChanged();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private class TareaDescargaDatos extends AsyncTask<String, Void, Void> {

        private boolean error = false;

        // Este método no puede acceder a la interfaz
        @Override
        protected Void doInBackground(String... urls) {

            InputStream is = null;
            String resultado = null;
            JSONObject json = null;
            JSONArray jsonArray = null;

            try {
                // Conecta con la URL y obtenemos el fichero con los datos
                HttpClient clienteHttp = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(urls[0]);
                HttpResponse respuesta = clienteHttp.execute(httpPost);
                HttpEntity entity = respuesta.getEntity();
                is = entity.getContent();

                // Lee el fichero de datos y genera una cadena de texto como resultado
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String linea = null;

                while ((linea = br.readLine()) != null)
                    sb.append(linea + "\n");

                is.close();
                resultado = sb.toString();

                json = new JSONObject(resultado);
                jsonArray = json.getJSONArray("features");

                String nombre = null;
                String descripcion = null;
                String categoria = null;
                String coordenadas = null;
                Monumento monumento = null;
                for (int i = 0; i < jsonArray.length(); i++) {
                    nombre = jsonArray.getJSONObject(i).getJSONObject("properties").getString("title");
                    descripcion = jsonArray.getJSONObject(i).getJSONObject("properties").getString("description");
                    categoria = jsonArray.getJSONObject(i).getJSONObject("properties").getString("category");
                    coordenadas = jsonArray.getJSONObject(i).getJSONObject("geometry").getString("coordinates");
                    coordenadas = coordenadas.substring(1, coordenadas.length() - 1);
                    String latlong[] = coordenadas.split(",");

                    monumento = new Monumento();
                    monumento.setNombre(nombre);
                    monumento.setDescripcion(descripcion);
                    monumento.setCategoria(categoria);
                    monumento.setLatitud(Float.parseFloat(latlong[0]));
                    monumento.setLongitud(Float.parseFloat(latlong[1]));
                    listaMonumentos.add(monumento);
                }

            } catch (ClientProtocolException cpe) {
                cpe.printStackTrace();
                error = true;
            } catch (IOException ioe) {
                ioe.printStackTrace();
                error = true;
            } catch (JSONException jse) {
                jse.printStackTrace();
                error = true;
            }

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            adapter.clear();
            listaMonumentos = new ArrayList<Monumento>();
        }

        @Override
        protected void onProgressUpdate(Void... progreso) {
            super.onProgressUpdate(progreso);

            adapter.notifyDataSetChanged();
        }

        @Override
        protected void onPostExecute(Void resultado) {
            super.onPostExecute(resultado);

            if (error) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                return;
            }

            adapter.notifyDataSetChanged();
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.datos_message), Toast.LENGTH_SHORT).show();
        }
    }

    private void cargarMonumentos() {
        TareaDescargaDatos tarea = new TareaDescargaDatos();
        tarea.execute(URL);
    }

    public boolean existeFavorito(String nombre, SQLiteDatabase db){
        String[] args = new String[]{nombre};
        Cursor c = db.rawQuery("SELECT nombre FROM favoritos WHERE nombre = ?", args);
        if(c.moveToFirst()){
            return true;
        }
        return false;
    }

    public void getFavs(){
        Cursor c = db.rawQuery("SELECT * FROM favoritos",null);
        /*if(c.moveToFirst()){
            do{
                String nombre = c.getString(0);
                favsArray.add(nombre);
            }while (c.moveToNext());
        }*/
        String name;
        if (c .moveToFirst()) {

            while (c.isAfterLast() == false) {
                name = c.getString(0);

                favsArray.add(name);
                c.moveToNext();
            }
        }
        for(String n : favsArray){
            for(Monumento monumento : listaMonumentos){
                if(monumento.getNombre().equals(n)){
                    listaFavsMonumentos.add(monumento);

                }
            }
        }
    }

    public void deleteFavs(String name){
        String[] args = new String[]{name};
        db.execSQL("DELETE FROM favoritos WHERE nombre = ?", args);

    }


   public void onItemClick(AdapterView<?> arg0, View view, int posicion, long id) {

        /*if (posicion == ListView.INVALID_POSITION)
            return;

        Monumento monumento = listaMonumentos.get(posicion);

        Intent i = new Intent(this, Mapa.class);
        i.putExtra("latitud", monumento.getLatitud());
        i.putExtra("longitud", monumento.getLongitud());
        i.putExtra("nombre", monumento.getNombre());
        startActivity(i);*/

    }

}
