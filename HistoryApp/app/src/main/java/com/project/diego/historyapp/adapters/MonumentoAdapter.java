package com.project.diego.historyapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.diego.historyapp.R;
import com.project.diego.historyapp.utils.Monumento;

import java.util.List;

/**
 * Created by Diego on 11/04/2016.
 */
public class MonumentoAdapter extends ArrayAdapter<Monumento>{

    private Context context;
    private int layoutId;
    private List<Monumento> datos;

    public MonumentoAdapter(Context context, int layoutId, List<Monumento> datos){
        super(context, layoutId, datos);
        this.context = context;
        this.layoutId = layoutId;
        this.datos = datos;
    }

    public View getView(int posicion, View view, ViewGroup padre){
        View fila = view;
        ItemMonumento item = null;

        if (fila == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            fila = inflater.inflate(layoutId, padre, false);

            item = new ItemMonumento();
            item.imagen = (ImageView) fila.findViewById(R.id.imageView);
            item.nombre = (TextView) fila.findViewById(R.id.tvNombre);
            item.direccion = (TextView) fila.findViewById(R.id.tvDireccion);


            fila.setTag(item);
        }
        else {
            item = (ItemMonumento) fila.getTag();
        }

        Monumento monumento = datos.get(posicion);
        item.nombre.setText(monumento.getNombre());
        item.direccion.setText(monumento.getDescripcion());
        return fila;
    }

    static class ItemMonumento {

        ImageView imagen;
        TextView nombre;
        TextView direccion;
    }
}
