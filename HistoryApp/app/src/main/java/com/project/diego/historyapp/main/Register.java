package com.project.diego.historyapp.main;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.project.diego.historyapp.R;
import com.project.diego.historyapp.utils.DataBase;

public class Register extends ActionBarActivity implements View.OnClickListener{

    DataBase db = null;
    SQLiteDatabase sqldb =null;
    User user = null;
    Intent intent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Button btReg = (Button) findViewById(R.id.bt_reg_reg);
        btReg.setOnClickListener(this);





    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_reg_reg:

                EditText name = (EditText) findViewById(R.id.tf_reg_user);
                EditText password = (EditText) findViewById(R.id.tf_reg_password);

                user = new User();
                user.setName(name.getText().toString());
                user.setPassword(password.getText().toString());

                db = new DataBase(this, "historyapp", null, 1);
                //sqldb = db.getWritableDatabase();

                int x = db.getUser(user.getName(), user.getPassword());

                if(x != 0){
                    Toast.makeText(this, R.string.mensaje_error_reg_datos, Toast.LENGTH_LONG).show();
                    return;
                }else{
                    db.newUser(user);
                    //db.close();
                    intent = new Intent(this, Login.class);
                    startActivity(intent);
                }
                break;
        }
    }
}
