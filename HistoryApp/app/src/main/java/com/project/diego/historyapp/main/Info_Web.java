package com.project.diego.historyapp.main;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.project.diego.historyapp.R;

import com.project.diego.historyapp.utils.Constantes;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Info_Web extends ActionBarActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Button btreg = (Button) findViewById(R.id.bt_reg_web);
        btreg.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_reg_web:
                EditText email =(EditText) findViewById(R.id.tf_email_reg);

                if(email.getText().toString().equals("")){
                    return;
                }

                String correo = email.getText().toString();

                    WebService webService = new WebService();
                    webService.execute(correo);

                break;
        }
    }

    private class WebService extends AsyncTask<String, Void, Void> {

        private ProgressDialog dialog;

        @Override
        protected Void doInBackground(String... params) {
            // Llamada a un Servicio Web con paso de parámetros
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getForObject(Constantes.SERVER_URL+Constantes.SERVER_SET_SPRING+params[0], Void.class);

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Info_Web.this);
            dialog.setTitle(R.string.mensaje_enviando);
            dialog.show();
        }


        protected void onPostExecute(Void aVoid) {

            EditText etTitulo = (EditText) findViewById(R.id.tf_email_reg);
            etTitulo.setText("");

            if (dialog != null)
                dialog.dismiss();
        }
    }

}
