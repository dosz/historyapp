package com.project.diego.historyapp.main;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.project.diego.historyapp.R;
import com.project.diego.historyapp.utils.DataBase;

public class Login extends ActionBarActivity implements View.OnClickListener{

    DataBase db = null;
    SQLiteDatabase sqldb =null;
    Intent intent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button btlogin = (Button) findViewById(R.id.bt_login);
        btlogin.setOnClickListener(this);
        Button btRegister = (Button) findViewById(R.id.bt_register);
        btRegister.setOnClickListener(this);

        db = new DataBase(this, "historyapp", null, 1);
        sqldb = db.getWritableDatabase();
    }


    @Override
    public void onClick(View v) {

        Intent intent = null;

        switch (v.getId()){
            case R.id.bt_login:
                EditText user = (EditText) findViewById(R.id.tf_login);
                EditText password = (EditText) findViewById(R.id.tf_password);

                int x = db.getUser(user.getText().toString(), password.getText().toString());

                if(user.equals("")||password.equals("")){
                    Toast.makeText(this, R.string.mensaje_error_login, Toast.LENGTH_LONG).show();
                    return;
                }

                if(x != 0){
                    intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(this, R.string.mensaje_error_login, Toast.LENGTH_LONG).show();
                    return;
                }
                break;
            case R.id.bt_register:
                intent = new Intent(this,Register.class);
                startActivity(intent);
                break;
        }
    }
}
