package com.project.diego.historyapp.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.project.diego.historyapp.main.User;

/**
 * Created by Diego on 14/04/2016.
 */
public class DataBase extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "historyapp.db";
    private static final int DATABASE_VERSION = 1;

    String sqlCreate = "CREATE TABLE users (name TEXT, password TEXT)";

    public DataBase(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS users");
        db.execSQL(sqlCreate);
    }

    public void newUser(User user){
        SQLiteDatabase sqdb = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("name", user.getName());
        values.put("password", user.getPassword());
        sqdb.insertOrThrow("users", null, values);
        sqdb.close();
    }

    public int getUser(String user, String password){
        SQLiteDatabase db = getReadableDatabase();

        String[] args = new String[]{user, password};
        Cursor c = db.rawQuery("SELECT * FROM users WHERE name = ? AND password = ?",args);
        int x = c.getCount();

        return x;

    }
}
