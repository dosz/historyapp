package com.project.diego.historyapp.main;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.webkit.WebView;

import com.project.diego.historyapp.R;

public class About extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        WebView webView= (WebView) findViewById(R.id.webView);
        webView.loadUrl("https://bitbucket.org/dosz/historyapp/wiki/Home");
    }

}
