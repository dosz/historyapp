package com.project.diego.historyapp.utils;

/**
 * Created by dos_6 on 15/12/2015.
 */
public class Monumento {

    private String nombre;
    private String direccion;
    private String categoria;
    private float latitud;
    private float longitud;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return direccion;
    }

    public void setDescripcion(String descripcion) {
        this.direccion = descripcion;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public float getLatitud() {
        return latitud;
    }

    public void setLatitud(float latitud) {
        this.latitud = latitud;
    }

    public float getLongitud() {
        return longitud;
    }

    public void setLongitud(float longitud) {
        this.longitud = longitud;
    }
}
